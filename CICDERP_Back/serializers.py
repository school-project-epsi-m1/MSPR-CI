import rest_framework.serializers
import CICDERP_Back.models as models


class ClientSerializer(rest_framework.serializers.ModelSerializer):
    """Serializer pour le model Client"""

    class Meta:
        model = models.Client
        fields = "__all__"

    client_number = rest_framework.serializers.UUIDField(
        read_only=True, required=False
    )
    first_name = rest_framework.serializers.CharField(max_length=200)
    last_name = rest_framework.serializers.CharField(max_length=200)
    email = rest_framework.serializers.EmailField(max_length=254)
    birthday = rest_framework.serializers.DateField()


class ProductSerializer(rest_framework.serializers.ModelSerializer):
    """Serializer pour le model product"""

    class Meta:
        model = models.Product
        fields = "__all__"

    reference = rest_framework.serializers.UUIDField(
        read_only=True, required=False
    )
    label = rest_framework.serializers.CharField(max_length=200)
    price = rest_framework.serializers.DecimalField(
        max_digits=10, decimal_places=2
    )


class OrderSerializer(rest_framework.serializers.ModelSerializer):
    """Serializer pour le model order"""

    class Meta:
        model = models.Order
        fields = ("order_number", "order_date", "total_ht", "client_id")

    order_number = rest_framework.serializers.UUIDField(read_only=True)
    client_id = rest_framework.serializers.UUIDField(write_only=True)


class OrderDetailSerializer(rest_framework.serializers.ModelSerializer):
    """Serializer pour le model productorder"""

    class Meta:
        model = models.ProductOrder
        fields = "__all__"
