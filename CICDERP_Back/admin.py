import django.contrib.admin as admin
import CICDERP_Back.models as models

# Register your models here.
admin.site.register(models.Client)
admin.site.register(models.Product)
admin.site.register(models.Order)
admin.site.register(models.ProductOrder)
