import rest_framework.response
import rest_framework.views as views
import CICDERP_Back.models as models
import CICDERP_Back.serializers as serializers
import django.db.models
import decimal
import typing


# Create your views here.


class ClientView(views.APIView):
    """
    Class API concernant les données clients avec un paramètre de clé primaire client
    """

    def get(self, request, pk: str) -> views.Response:
        """
        Fonction get prenant en paramètre une clé primaire client et retournant des orders serialisés

        :param request: contenu de la request get
        :param pk: clé primaire d'un client au format UUID
        :return: Response serializé d'orders
        """
        orders = models.Order.objects.filter(client_id=pk)
        serializer = serializers.OrderSerializer(orders, many=True)
        return views.Response(serializer.data)

    def put(self, request, pk: str) -> views.Response:
        """
        Fonction put prenant en paramètre une clé primaire client et met à jour ce client

        :param request: contenu de la request put
        :param pk: clé primaire d'un client au format UUID
        :return: Response serializé dun client ou un code erreur
        """
        client1 = models.Client.objects.get(pk=pk)
        serializer = serializers.ClientSerializer(client1, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return views.Response(serializer.data)
        return views.Response(
            serializer.errors, status=views.status.HTTP_400_BAD_REQUEST
        )


class ClientListView(views.APIView):
    """
    Class API concernant les données clients sans paramètre d'entré
    """

    def get(self, request) -> views.Response:
        """
        Fonction get retournant la liste des clients

        :param request: contenu de la request get
        :return: Response sérialisé de clients.
        """
        client1 = models.Client.objects.all()
        serializer = serializers.ClientSerializer(client1, many=True)
        return views.Response(serializer.data)

    def post(self, request) -> views.Response:
        """
        Fonction post pour créer un client

        :param request: contenu de la request post
        :return: Response d'une code status
        """
        serializer = serializers.ClientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return views.Response(
                serializer.data, status=views.status.HTTP_201_CREATED
            )
        return views.Response(
            serializer.errors, status=views.status.HTTP_400_BAD_REQUEST
        )


class ProductView(views.APIView):
    """
    Class API concernant les données produits avec un paramètre de clé primaire produit
    """

    def get(self, request, pk: str) -> views.Response:
        """
        Fonction get prenant en paramètre une clé primaire produit et retournant un produit

        :param request: contenu de la request get
        :param pk: clé primaire d'un produit au format UUID
        :return: Response serializé d'un produit
        """
        product1 = models.Product.objects.get(pk=pk)
        serializer = serializers.ProductSerializer(product1)
        return views.Response(serializer.data)

    def put(self, request, pk: str) -> views.Response:
        """
         Fonction put prenant en paramètre une clé primaire produit

        :param request: contenu de la request put
        :param pk: clé primaire d'un produit au format UUID
        :return: Response serializé d'un produit ou un code erreur
        """
        product1 = models.Product.objects.get(pk=pk)
        serializer = serializers.ProductSerializer(product1, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return views.Response(serializer.data)
        return views.Response(
            serializer.errors, status=views.status.HTTP_400_BAD_REQUEST
        )


class ProductListView(views.APIView):
    """
    Class API concernant les données produits sans paramètre d'entré
    """

    def get(self, request) -> views.Response:
        """
        Fonction get retournant la liste des produits

        :param request: contenu de la request get
        :return: Response serialisé de produits
        """
        product1 = models.Product.objects.all()
        serializer = serializers.ProductSerializer(product1, many=True)
        return views.Response(serializer.data)

    def post(self, request) -> views.Response:
        """
        Fonction post pour créer un produit

        :param request: contenu de la request post
        :return: Response un status
        """
        serializer = serializers.ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return views.Response(
                serializer.data, status=views.status.HTTP_201_CREATED
            )
        return views.Response(
            serializer.errors, status=views.status.HTTP_400_BAD_REQUEST
        )


class OrderView(views.APIView):
    """
    Class API concernant les données order avec un paramètre de clé primaire order
    """

    def get(self, request, pk: str) -> views.Response:
        """
        Fonction get prenant en paramètre une clé primaire order et retournant des produitsorders

        :param request: contenu de la request get
        :param pk: clé primaire d'un order au format UUID
        :return: Response serializé de produitorder
        """
        order_detail = models.ProductOrder.objects.filter(order=pk)
        serializer = serializers.OrderDetailSerializer(order_detail, many=True)
        return views.Response(serializer.data)


class OrderListView(views.APIView):
    """
    Class API concernant les données orders sans paramètre d'entré
    """

    def get(self, request) -> views.Response:
        """
         Fonction get retournant la liste des orders

        :param request: contenu de la request get
        :return: Response serializé d'orders
        """
        order1 = models.Order.objects.all()
        serializer = serializers.OrderSerializer(order1, many=True)
        return views.Response(serializer.data)

    def post(self, request) -> views.Response:
        """
        Fonction post pour créer un order

        :param request: contenu de la request post
        :return: Response un status
        """
        serializer = serializers.OrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return views.Response(
                serializer.data, status=views.status.HTTP_201_CREATED
            )
        return views.Response(
            serializer.errors, status=views.status.HTTP_400_BAD_REQUEST
        )


class CAViews(views.APIView):
    """
    Class view de retour du chiffre d'affaire
    """

    def get(self, request) -> views.Response:
        """
        Fonction pour récupérer la somme des commmandes

        :param request: contenu de la request get
        :return: response d'un json
        """
        orders = models.Order.objects.values("total_ht").aggregate(
            django.db.models.Sum("total_ht")
        )
        sum_ht = orders["total_ht__sum"] if orders["total_ht__sum"] else 0
        tva = 20
        ttc = sum_ht * decimal.Decimal(1 + (20 / 100))
        return views.Response({"CA_HT": sum_ht, "TVA": tva, "TTC": ttc})
