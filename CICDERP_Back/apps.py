import django.apps


class CicderpBackConfig(django.apps.AppConfig):
    name = "CICDERP_Back"
