import datetime
import decimal

import django.test
import CICDERP_Back.models as models
import rest_framework.test as test
import rest_framework.status as status


# Create your tests here.
class APITestCase(test.APITestCase):
    """Test cases des interfaces de l'API"""

    def setUp(self) -> None:
        """
        setUp avant de lancer les tests

        :return: None
        """
        self.user = models.Client.objects.create(
            first_name="test_first_name",
            last_name="test_last_name",
            email="email@email.com",
            birthday=datetime.date(1990, 12, 20),
        )
        self.order1 = models.Order.objects.create(
            total_ht=150, client=self.user
        )
        self.order2 = models.Order.objects.create(
            total_ht=200, client=self.user
        )
        self.order3 = models.Order.objects.create(
            total_ht=300, client=self.user
        )

    def test_create_client(self) -> None:
        """
        Test de création d'un client

        :return: None
        """
        data = {
            "first_name": "test",
            "last_name": "test",
            "email": "test@test.com",
            "birthday": "2000-01-01",
        }
        response = self.client.post("/client/", data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_order(self) -> None:
        """
        Test de création d'un order

        :return: None
        """
        data = {
            "total_ht": "587.00",
            "client_id": str(self.user.client_number),
        }
        response = self.client.post("/order/", data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_product(self) -> None:
        """
        Test de création d'un product

        :return: None
        """
        data = {"label": "test_label_product", "price": 120}
        response = self.client.post("/product/", data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_client_order(self) -> None:
        """
        Test de récupération des orders d'un client

        :return: None
        """
        response = self.client.get("/client/" + str(self.user.client_number))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data[0]["order_number"], str(self.order1.order_number)
        )

    def test_update_client(self) -> None:
        """
        Test d'update des informations d'un client

        :return: None
        """
        data = {
            "first_name": "test",
            "last_name": "test",
            "email": "test@test.com",
            "birthday": "2000-01-01",
        }
        response = self.client.put(
            "/client/" + str(self.user.client_number), data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["first_name"], "test")

    def test_get_ca(self) -> None:
        """
        Test de récupération du Chiffre d'Affaire

        :return: None
        """
        response = self.client.get("/ca/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["CA_HT"], 650)


class ClientTestCase(django.test.TestCase):
    """Tests case pour les clients"""

    def setUp(self) -> None:
        """
        setUp avant de lancer les tests

        :return: None
        """
        models.Client.objects.create(
            first_name="test_first_name",
            last_name="test_last_name",
            email="email@email.com",
            birthday=datetime.date(2000, 1, 1),
        )

    def test_client_exist(self) -> None:
        """
        Test de vérification de la présence du client

        :return: None
        """
        client = models.Client.objects.get(first_name="test_first_name")
        self.assertEqual(isinstance(client, models.Client), True)


class ProductTestCase(django.test.TestCase):
    """Tests case pour les products"""

    def setUp(self) -> None:
        """
        setUp avant de lancer les tests

        :return: None
        """
        models.Product.objects.create(
            label="test_label", price=decimal.Decimal(1234)
        )

    def test_product_exist(self) -> None:
        """
        Test de vérification de la présence du product

        :return: None
        """
        product = models.Product.objects.get(label="test_label")
        self.assertEqual(isinstance(product, models.Product), True)


class OrderTestCase(django.test.TestCase):
    """Tests case pour les orders"""

    def setUp(self) -> None:
        """
        setUp avant de lancer les tests

        :return: None
        """
        models.Client.objects.create(
            first_name="test_first_name",
            last_name="test_last_name",
            email="email@email.com",
            birthday=datetime.date(2000, 1, 1),
        )
        models.Order.objects.create(
            total_ht=500,
            client=models.Client.objects.get(first_name="test_first_name"),
        )

    def test_order_exist(self) -> None:
        """
        Test de vérification de la présence de l'order

        :return: None
        """
        order = models.Order.objects.get(
            client=models.Client.objects.get(first_name="test_first_name")
        )
        self.assertEqual(isinstance(order, models.Order), True)
