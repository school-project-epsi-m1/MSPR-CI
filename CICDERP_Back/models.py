import django.db.models
import uuid
import django.utils.timezone as timezone


# Create your models here.


class Client(django.db.models.Model):
    """Class de client"""

    client_number = django.db.models.UUIDField(
        default=uuid.uuid4, primary_key=True
    )
    first_name = django.db.models.CharField(max_length=200)
    last_name = django.db.models.CharField(max_length=200)
    email = django.db.models.EmailField(max_length=254)
    birthday = django.db.models.DateField()


class Product(django.db.models.Model):
    """Class de produit"""

    reference = django.db.models.UUIDField(
        default=uuid.uuid4, primary_key=True
    )
    label = django.db.models.CharField(max_length=200)
    price = django.db.models.DecimalField(max_digits=10, decimal_places=2)


class Order(django.db.models.Model):
    """Class de commande"""

    order_number = django.db.models.UUIDField(
        default=uuid.uuid4, primary_key=True
    )
    order_date = django.db.models.DateTimeField(default=timezone.now)
    total_ht = django.db.models.DecimalField(max_digits=10, decimal_places=2)
    client = django.db.models.ForeignKey(
        Client, related_name="orders", on_delete=django.db.models.CASCADE
    )
    products = django.db.models.ManyToManyField(
        Product, through="ProductOrder"
    )


class ProductOrder(django.db.models.Model):
    """Class d'association entre order et commande"""

    product = django.db.models.ForeignKey(
        Product, on_delete=django.db.models.CASCADE
    )
    order = django.db.models.ForeignKey(
        Order, related_name="orders", on_delete=django.db.models.CASCADE
    )
    quantity = django.db.models.IntegerField()
