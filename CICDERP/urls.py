"""
urls conf
"""
import django.contrib.admin as admin
import django.urls
import django.conf.urls

import CICDERP_Back.views

import rest_framework.permissions as permissions
import drf_yasg.views
import drf_yasg.openapi as openapi

schema_view = drf_yasg.views.get_schema_view(
    openapi.Info(
        title="Snippets API",
        default_version="v1",
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    django.conf.urls.url(
        r"^api(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    django.conf.urls.url(
        r"^api/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    django.conf.urls.url(
        r"^redoc/$",
        schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc",
    ),
    django.urls.path("admin/", admin.site.urls),
    django.urls.path("client/", CICDERP_Back.views.ClientListView.as_view()),
    django.urls.path(
        "client/<str:pk>", CICDERP_Back.views.ClientView.as_view()
    ),
    django.urls.path("product/", CICDERP_Back.views.ProductListView.as_view()),
    django.urls.path(
        "product/<str:pk>", CICDERP_Back.views.ProductView.as_view()
    ),
    django.urls.path("order/", CICDERP_Back.views.OrderListView.as_view()),
    django.urls.path("order/<str:pk>", CICDERP_Back.views.OrderView.as_view()),
    django.urls.path("ca/", CICDERP_Back.views.CAViews.as_view()),
]
